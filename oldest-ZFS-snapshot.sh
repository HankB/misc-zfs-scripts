#!/usr/bin/env bash
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

# list oldest snapshots for each ZFS dataset

# Usage
# oldest-ZFS-snapshot 
# oldest-ZFS-snapshot  | sort # for a list sorted oldest to newest|none

for dataset in $(/sbin/zfs list -H -o name)
do
    oldest=$(/sbin/zfs list -t snap "$dataset" -s creation -H -o creation,name|head -1) || true
    if [ -n "$oldest" ] # not all datasets have snapshots
    then
        datestamp=$(echo "$oldest"|cut -c 1-21)
        datestamp=$(date -d"$datestamp" +%Y-%m-%d)
        name=$(echo "$oldest"|cut -c 22-)
        echo "$datestamp $name"
    else
        echo "                $dataset"
    fi
done
