# Misc-ZFS scripts

Miscellaneous scripts to support my usage of ZFS

## Motivation

Save typing for common tasks.

## Integrate with your tools

*???*

- [ ] [Set up project integrations](https://gitlab.com/HankB/misc-zfs-scripts/-/settings/integrations)

## Collaborate with your team 

*My team? Looking for volunteers!*

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

*I need to look into this.*

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name

Miscellaneous ZFS scripts.

## Description

See Motivation

## Badges

*TBD*

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Installation

Copy to some convenient location in your path.

## Usage

```text
count-ZFS-snapshots.sh`             # No anticipated arguments. Yet.
count-ZFS-snapshots.sh` | sort -g   # for a sorted list.
oldest-ZFS-snapshot.sh              # possible command line arguments - filesystems to check
oldest-ZFS-snapshot.sh  | sort      # sort oldest to newest.
```

## Support

File an issue.

## Roadmap

I will add scripts as I identify the need for automating tasks.

## Contributing

Please feel free to ask for better descriptions, improvements in the scripts, corrections for typos and so on. I'm generally open to contributions that stick with the spirit of what I'm doing and meet some minimal quality standards.

## Authors and acknowledgment

* Hank Barta

## License

 Apache License, Version 2.0, January 2004

## Project status

* `count-ZFS-snapshots.sh` initial tests working and `shellcheck` clean. Added total count of snapshots. This will overstate the actual number of snapshots due to snapshots for recursive datasets being counted more than once.
* `oldest-ZFS-snapshot.sh` List the oldest ZFS snapshot for each dataset. (or leave the date field blank when there are no snapshots.)

## Testing and QC

At a miminum

* Pass `shellcheck` w/out complaint
* Use the "boilerplate" from <https://kvz.io/bash-best-practices.html>
* Employ unit tests (`shunit2`) where appropriate, but probably not needed for these scripts.
