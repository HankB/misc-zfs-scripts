#!/usr/bin/env bash
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

# Count snapshots for each ZFS dataset

# Usage
# count-ZFS-snapshots.sh 
# count-ZFS-snapshots.sh  | sort -g # for a list sorted by count

## prepare to sum, count
total=0
filesystems=0

for dataset in $(/sbin/zfs list -H -o name)
do
    count=$(/sbin/zfs list -t snap "$dataset" 2>/dev/null|wc -l)
    printf '%5.5s %s\n' "$count" "$dataset"
    (( "total = $total + $count" )) || true
    (( "filesystems = $filesystems + 1" )) || true
done

echo "$total snapshots found in $filesystems filesystems."